#+title: selime.el -- SLIME emulation for Emacs Lisp
#+author: Jamie Beardslee
#+email: jdb@jamzattack.xyz

This package is my attempt at recreating some features of [[https://github.com/slime/slime][SLIME]] for
Elisp.

* Installation
** package.el

Download selime.el, and use =M-x package-install-file RET
path/to/selime.el=.

** straight.el

Evaluate the following:

#+begin_src emacs-lisp
  (straight-use-package
   '(selime :type git
	    :flavor melpa
	    :repo "git://jamzattack.xyz/selime"))
#+end_src

** Manual installation

1. Download selime.el
2. Stick it in [[help:load-path][load-path]]
3. Update autoloads
4. (optionally) byte-compile it

* Features

** Compilation

Selime adds the following keybindings for byte-compilation:

| C-c C-c | Compile top-level form                        |
| C-c C-e | Compile last s-expression                     |
| C-c M-k | Compile the buffer's file, without loading it |
| C-c C-k | Compile and load the current buffer           |

** Documentation

Selime binds the following keys for documentation:

Note: If the package [[https://github.com/Wilfred/helpful][helpful]] is installed, selime uses helpful's
variants of these functions.

| C-c C-d C-d | Describe symbol   |
| C-c C-d d   | Describe symbol   |
| C-c C-d C-f | Describe function |
| C-c C-d f   | Describe function |
| C-c C-d C-v | Describe variable |
| C-c C-d v   | Describe variable |

** IELM

Selime defines a function, [[help:selime-ielm][selime-ielm]], which acts more like
[[help:slime-switch-to-output-buffer][slime-switch-to-output-buffer]].  With a prefix argument (C-u), you can
select the name for the IELM buffer.

| C-c C-z | Switch to IELM in other window |

** Disassembly

If point is on a function, disassemble it.  Otherwise, prompt for a
function name.

| C-c M-d | Disassemble function at point |

** Tracing

Like SLIME, =C-c C-t= toggles tracing of a function.  The function
name is read from the minibuffer, with the symbol at point being the
default input.

| C-c C-t | Toggle trace |

** Macroexpand

Selime implements a very minimal (i.e. crap) form of macro expansion.
The command [[help:selime-macroexpand][selime-macroexpand]] will open a new buffer containing the
expansion of the s-expression around point.

| C-c RET | Macroexpand sexp around point |

*** TODO Integrate with [[https://github.com/joddie/macrostep][macrostep]]

* License

GPL3+
