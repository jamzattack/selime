;;; selime.el --- A better version of elisp-slime-nav  -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Jamie Beardslee

;; Author: Jamie Beardslee <jdb@jamzattack.xyz>
;; URL: https://git.jamzattack.xyz/selime
;; Keywords: lisp
;; Version: 2020.07.03

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; My attempt at emulating slime-mode for Emacs Lisp.  A bunch of
;; things work, but there are still a couple of functions to add and
;; improve.

;;; Code:

(require 'disass)
(require 'trace)
(require 'ielm)
(require 'help-fns)
(require 'pp)
(when (featurep 'helpful)
  (require 'helpful))

;;; Help

;;;###autoload
(defun selime-describe-function ()
  "If package \"helpful\" is installed, call `helpful-callable',
otherwise call `describe-function'"
  (interactive)
  (if (featurep 'helpful)
      (call-interactively #'helpful-callable)
    (call-interactively #'describe-function)))

;;;###autoload
(defun selime-describe-variable ()
  "If package \"helpful\" is installed, call `helpful-variable',
otherwise call `describe-variable'"
  (interactive)
  (if (featurep 'helpful)
      (call-interactively #'helpful-variable)
    (call-interactively #'describe-variable)))

;;;###autoload
(defun selime-describe-symbol ()
  "If package \"helpful\" is installed, call `helpful-at-point',
otherwise call `describe-symbol' on the symbol at point"
  (interactive)
  (if (featurep 'helpful)
      (call-interactively #'helpful-at-point)
    (describe-symbol (symbol-at-point))))

;;; Compilation

;;;###autoload
(defun selime-compile-file (&optional file)
  "Compiles the the current file without loading it.

With prefix arg, read FILE name from minibuffer."
  (interactive (list (if current-prefix-arg
			 (read-file-name "Compile file: ")
		       (buffer-file-name (current-buffer)))))
  (if file
      (byte-compile-file file)
    (user-error "Buffer is not associated with a file")))

;;;###autoload
(defun selime-compile-and-load-file (&optional file)
  "Compiles either the file that the current buffer is visiting,
or the current buffer if it isn't visiting a file.  With prefix
arg, read file name from minibuffer."
  (interactive (list (if current-prefix-arg
			 (read-file-name "Compile and load file: ")
		       (buffer-file-name (current-buffer)))))
  (save-excursion
    (if file
	(byte-compile-file file t)
      (eval-buffer (byte-compile-from-buffer (current-buffer))))))

;;;###autoload
(defun selime-compile-last-sexp (&optional insert)
  "Compile and evaluate the last sexp.

Print the result in the echo area.  With prefix argument, INSERT
value in current buffer after the form."
  (interactive "P")
  (declare (interactive-only))
  (let ((value (eval
		(byte-compile-sexp
		 (elisp--preceding-sexp))
		lexical-binding)))
    (if arg
	(prin1 value (current-buffer))
      (message "%s" (prin1-to-string value)))))

;;; Tracing

(defvar selime-trace-buffer
  "*selime trace*")

(defun selime-toggle-trace (function)
  "Toggle trace for FUNCTION."
  (interactive (list
		(intern
		 (read-string "(Un)trace: "
			      (symbol-name
			       (symbol-at-point))))))
  (if (trace-is-traced function)
      (untrace-function function)
    (trace-function function selime-trace-buffer)))

;;; Misc

;;;###autoload
(defun selime-macroexpand ()
  "Macro expand the preceding sexp.

This will show the expanded form in the \"*selime macroexpand*\"
buffer.  If point is already in the macroexpand buffer, replace
the sexp with its expansion."
  (interactive)
  (let ((buffer (get-buffer-create "*selime macroexpand*"))
	(sexp (elisp--preceding-sexp)))
    (if (eq (current-buffer) buffer)
	(save-excursion
	  (let ((bounds (bounds-of-thing-at-point 'sexp)))
	    (delete-region (car bounds) (1+ (cdr bounds))))
	  (insert (pp-to-string (macroexpand-1 sexp))))
      (pp-display-expression (macroexpand-1 sexp)
			     buffer))
    (with-current-buffer buffer
      (indent-region (point-min) (point-max)))))

;;;###autoload
(defun selime-disassemble (function)
  "If point is on a FUNCTION, disassemble it.

Otherwise, prompt for a function to disassemble."
  (interactive (list (let ((fun (symbol-at-point)))
		       (if (fboundp fun)
			   fun
			 (intern
			  (completing-read "Disassemble function: " obarray 'fboundp t nil nil ))))))
  (disassemble function))

;;;###autoload
(defun selime-ielm (&optional buffer-name)
  "Open IELM in another window.

With prefix arg BUFFER-NAME, prompt for the name of the new IELM
buffer."
  (interactive (list (when current-prefix-arg
		       (read-string "IELM Buffer Name: "))))
  (let ((buffer (get-buffer-create (or buffer-name "*ielm*"))))
    (switch-to-buffer-other-window buffer)
    (inferior-emacs-lisp-mode)))

;;; Selime-mode

(defvar selime-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-d C-d")	'selime-describe-symbol)
    (define-key map (kbd "C-c C-d d")	'selime-describe-symbol)
    (define-key map (kbd "C-c C-d C-f")	'selime-describe-function)
    (define-key map (kbd "C-c C-d f")	'selime-describe-function)
    (define-key map (kbd "C-c C-d C-v")	'selime-describe-variable)
    (define-key map (kbd "C-c C-d v")	'selime-describe-variable)
    (define-key map (kbd "C-c C-c")	'compile-defun)
    (define-key map (kbd "C-c M-d")	'selime-disassemble)
    (define-key map (kbd "C-c C-m")	'selime-macroexpand)
    (define-key map (kbd "C-c C-k")	'selime-compile-and-load-file)
    (define-key map (kbd "C-c M-k")	'selime-compile-file)
    (define-key map (kbd "C-c C-z")	'selime-ielm)
    (define-key map (kbd "C-c C-t")	'selime-toggle-trace)
    (define-key map (kbd "C-c C-e")	'selime-compile-last-sexp)
    map))

;;;###autoload
(define-minor-mode selime-mode
  "Enable Slime-style keybindings for elisp buffers.

\\{selime-mode-map}"
  nil "" selime-mode-map)

(provide 'selime)
;;; selime.el ends here
